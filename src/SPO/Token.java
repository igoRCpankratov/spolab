/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package SPO;


import SPO.Lexer.TokenType;



public class Token {
    public TokenType tokenType;
    public String value;

    Token(TokenType t, String v) {
        tokenType = t;
        value = v;
    }


    public String toString() {
        return ("<" + tokenType.name() + "> " + value);
    }
}

