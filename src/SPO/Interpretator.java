/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package SPO;

import SPO.Token;
import SPO.Lexer;
import SPO.Parser;
import java.util.ArrayList;
import SPO.OutputClass;

public class Interpretator {

    
    public static void main(String[] args) {
        OutputClass.init();
    	String fileInput = "input.txt";    	
        ArrayList<Token> tokens = Lexer.getList(fileInput);
        Parser parser = new Parser(tokens);
        parser.parse(fileInput);
        OutputClass.saveList(tokens);
    }
    
}
