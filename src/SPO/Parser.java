

package SPO;


import java.util.ArrayList;
import java.util.ListIterator;

import SPO.Inter;

public class Parser {
    ListIterator<Token> list;
    boolean expFound;

    Parser(ArrayList<Token> newList) {
        list = newList.listIterator();
        expFound = false;
    }

    String nextToken() {
        if (!list.hasNext())
               return "EOF";

        return list.next().tokenType.name();
    }

    boolean exp() {
        if (!expFound) {
            expFound = true;
            list.previous();
            OutputClass.outStd("ERROR: " + "("+ (list.nextIndex()+1) +")" + nextToken());
        }

        return false;
    }

    boolean checkFunction() {
       	if (nextToken().equals("VARIABLE"))
       		if (nextToken().equals("STREND"))
       			return true;
       		else return exp();
        return exp();
    }
    boolean checkMaths() {
    	if (nextToken().equals("EQUALS")){
    		String token = nextToken();
        	while(!token.equals("STREND")){	
	        	if(token.equals("OPNBRACKET"))
	        		if(!checkMathsSK()) 
	        			return false;
	        		else {
	        			token = nextToken();	
	        			if(token.equals("MATH")){
	        				token = nextToken();
	        				continue;
	        			}
	        			continue;
	        		}
	        	if(token.equals("VARIABLE") || token.equals("NUMBER")){
	        		token = nextToken();
	        		if(token.equals("STREND"))
	        			return true;
	        		if(token.equals("MATH")){
	        			if(nextToken().equals("STREND")){
	        				return exp();
	        			} else {
	        				list.previous();
	        			}
	        			token = nextToken();
	        			continue;
	        		}
	        		else 
	        			return exp();
	        	} else
	        		return exp();	
	        	//token = nextToken();
        	}
        	if(token.equals("STREND")) return true;
    	} else
    		return exp();
        return true;
    }
    boolean checkMathsSK() {
    	String token = nextToken();
    	if(token.equals("VARIABLE") || token.equals("NUMBER"))
    		while(!token.equals("CLSBRACKET")){
    			token = nextToken();
    			if(token.equals("OPNBRACKET")){
    				if(!checkMathsSK()) 
	        			return false;
    			}
    			if(token.equals("MATH")){
    				token = nextToken();
    				if(token.equals("OPNBRACKET")){
        				if(!checkMathsSK()) 
    	        			return false;
        				else continue;
        			}
        			if(token.equals("VARIABLE") || token.equals("NUMBER")){
        				continue;
    					}
    				else
    					{
    					
    						return exp();
    					}
    			} else 
    				if(token.equals("CLSBRACKET")) return true;
    				else {
    					return exp();
    			}
    		}
    	if(token.equals("CLSBRACKET")) return true;
    	else 
    		return exp();

    }




    boolean body() {
        if (nextToken().equals("START")){
            String token = nextToken();
            while (!token.equals("FINISH") && !token.equals("EOF")) {
                if(token.equals("INTEGER") || token.equals("ANSWER"))
                	if(!checkFunction()) return false;
                	else {
                		token = nextToken();
                		continue;
                	}
                if(token.equals("VARIABLE"))
                	if(!checkMaths()) return false;
                	else {
                		token = nextToken();
                		continue;
                	}
            }
            if(token.equals("EOF")){
            	list.previous();
            	//list.previous();
            	if(!nextToken().equals("FINISH")){
            		OutputClass.outStd("Finish NOT FOUND");
            		return false;
            	}
            }
        }
        return true;
    }

    public void parse(String fileInput) {
        if (body()){
//            OutputClass.outDbg("\nSyntax is correct");
            Inter inter = new Inter();
            inter.start(fileInput);
        }
        
    }
}
